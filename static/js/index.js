const noTodos = document.querySelector(".no-todos");
// console.log(noTodos, "xxxx");
// noTodos.style.display = "none";
let button = document.querySelector("button");
let input = document.querySelector("input");
let deleteButton = document.querySelectorAll(".delete");
const form = document.querySelector("form");
// button.remove();
// button.disabled = false;
// button.style.backgroundColor = "yellow";
const ul = document.querySelector("ul");

form.addEventListener("submit", async (e) => {
    e.preventDefault();
    const data = new FormData(e.target).entries()

    const response = await fetch("http://localhost:5000/todos", {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(Object.fromEntries(data)),
    })
    const newTodo = await response.json();
    addTodo(newTodo);

})

// click, keyup, scroll, focus, change
input.addEventListener("keyup", (e) => {
    let value = e.target.value;
    // if (value && value.length > 0) {
    // if (value?.length > 0) {
    //     button.disabled = false;
    // } else {
    //     button.disabled = true;
    // }
    button.disabled = value?.length < 1;
});

async function removeTodo(e) {
    if (e.target.nodeName === "SPAN") {
        const li = e.target.parentElement;
        const id = e.target.getAttribute("data-id");
        await fetch(`http://localhost:5000/remove/${id}`, {
            method: "DELETE",
        })

        li.remove();

        const list = document.querySelectorAll("li");

        if (list.length === 0) {
               noTodos.classList.remove("hidden");
        }
    }
}

function attachEvents() {
    const lis = document.querySelectorAll("li");

    lis.forEach((li) => {
        li.removeEventListener("click", removeTodo);
        li.addEventListener("click", removeTodo);
    })
}

function createIcon() {
    const span = document.createElement("span");
    span.classList.add("icon", "material-symbols-outlined");
    span.innerText = "close";
    return span;
}

function addTodo(newTodo) {
    const li = document.createElement("li");
    li.classList.add("list-group-item", "d-flex", "justify-content-between");
    const span = document.createElement("span");
    span.innerText = newTodo.label;
    const icon = createIcon();
    icon.setAttribute("data-id", newTodo.id);

    li.appendChild(span);
    li.appendChild(icon);

    ul.appendChild(li);
    input.value = "";
    button.disabled = true;
    noTodos.classList.add("hidden");

    attachEvents();
}

attachEvents()