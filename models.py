import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:123456@localhost:5432/etoro"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)


class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column("label", db.Text(), nullable=False)
    status = db.Column("status", db.Text(), nullable=False)
    created = db.Column("created_at", db.DateTime, default=datetime.datetime.now)


with app.app_context():
    db.create_all()
