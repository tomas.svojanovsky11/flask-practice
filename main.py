from flask import render_template, request, url_for, redirect
from models import app, db, Todo
from flask_cors import CORS

cors = CORS(app)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/todos", methods=["GET"])
def todo_list():
    if request.form:
        new_todo = Todo(label=request.form["label"], status="created")
        db.session.add(new_todo)
        db.session.commit()

    todos = Todo.query.all()
    return render_template("todos.html", todos=todos)


@app.route("/todos", methods=["POST"])
def todo_list_rest():
    body = request.json
    new_todo = Todo(label=body.get("label"), status="created")
    db.session.add(new_todo)
    db.session.commit()

    return {"id": new_todo.id, "label": new_todo.label}, 201


@app.route("/remove/<id>", methods=["GET"])
def todo_delete(id):
    todo = Todo.query.get(id)
    db.session.delete(todo)
    db.session.commit()
    return redirect(url_for("todo_list"))


@app.route("/remove/<id>", methods=["DELETE"])
def todo_delete_rest(id):
    todo = Todo.query.get(id)
    db.session.delete(todo)
    db.session.commit()
    return "", 203


if __name__ == "__main__":
    app.run(debug=True)

